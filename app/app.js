Drupal.behaviors.wtphSurvey = {
    attach: function(context, settings) {
        var wtphSurvey = this;

        var QuestionModel = Backbone.Model.extend({
            view: {},

            initialize: function(){
                this.view = new QuestionView({model: this});
            }
        });

        var QuestionView = Backbone.View.extend({

            render: function() {
                var path = this.model.get('path');
                var id = this.model.id;
                var hiddenClass = this.model.id != 'q1' ? 'hidden' : '';
                var wrapperPrefix = '<div class="question question-'+ id +' ' + hiddenClass +'" data-id="'+ id +'">';
                var questionHeading = '<h2>'+ this.model.get('question') +'</h2>';
                var wrapperSuffix = '</div>';
                var elementPrefix = '<div class="form-element">';
                var elementSuffix = '</div>';

                var elements = '';
                _.each(this.model.get('form_group'), function(group) {
                    switch (group.type) {
                        case 'text':
                            elements += elementPrefix +
                            '<input type="'+ group.type +'" name="'+ group.name +'">' + elementSuffix;
                            break;
                        case 'radio':
                            _.each(group.options, function(option){
                                elements += elementPrefix +
                                    '<input type="'+ group.type +'" name="'+ option.name +'" value="'+ option.value +'"> ' + option.label +
                                    elementSuffix;
                            });
                            break;
                        case 'checkbox':
                            _.each(group.options, function(option){
                                elements += elementPrefix +
                                    '<input type="'+ group.type +'" name="'+ option.name +'" value="'+ option.value +'"> ' + option.label +
                                    elementSuffix;
                            });
                            break;
                        case 'textarea':
                            elements += elementPrefix +
                            '<textarea name="'+ group.name +'"></textarea>' + elementSuffix;
                            break;
                    }
                });

                var buttons = '<div class="form-element">';
                    if(!_.isUndefined(path.prev)){
                        buttons += '<button class="button  button-prev" type="button" data-prev="'+ path.prev.default +'">'+
                            path.prevText +'</button>';
                    }
                    if(!_.isUndefined(path.next)){
                        buttons += '<button class="button  button-next" type="button" data-next="'+ path.next.default +'">'+
                            path.nextText +'</button>';
                    }
                    buttons += '</div>';

                elements += buttons;

                var template = wrapperPrefix + questionHeading + elements + wrapperSuffix;
                this.$el.html(template);

                return this;
            },

        });

        var QuestionCollection = Backbone.Collection.extend({
            model: QuestionModel
        });

        var App = Backbone.View.extend({
            el: '#wtph-survey',

            tagName: '#wtph-survey',

            questionCollection: {},

            events: {
                'change input,textarea': 'answer',
                'click .button': 'proceed'
            },

            initialize: function(){
                var _self = this;

                //get question data
                $.when(wtphSurvey.getResource('app/data.json')).done(function(data) {
                    _self.questionCollection = new QuestionCollection(data);

                    // get template
                    $.when(wtphSurvey.getResource('app/tpl/app.html')).done(function(data) {
                        _self.template = _.template(data);

                        _self.render();
                    });
                });
            },

            render: function(){
                this.$el.html(this.template());

                var appView = this.$el;

                _.each(this.questionCollection.models, function(question, index){
                    appView.find('#survey-form').append(question.view.render().$el.html());
                });

            },

            answer: function(event) {
                event.preventDefault();

                var input = $(event.currentTarget);
                var type = input.attr('type');
                var key = input.attr('name');
                var value = input.val();

                if(type == 'checkbox') {
                    var storedValue = this.model.get(key);

                    if(input.prop( "checked" )){
                        if(_.isUndefined(storedValue)){
                            this.model.set(key, [value]);
                        }else {
                            storedValue.push(value);
                            this.model.set(key, _.uniq(storedValue));
                        }
                    }else {
                        this.model.set(key, _.without(storedValue, value));
                    }
                }else{
                    this.model.set(key, value);
                }

                this.setMarkers(key, value);
            },

            // set previous and next questions markers
            setMarkers: function(currentQuestionID, answer){
                if(_.isUndefined(currentQuestionID) || _.isUndefined(answer))
                    return;

                var question = this.questionCollection.findWhere({id: currentQuestionID});
                if(_.isUndefined(question))
                    return;

                var path = question.get('path') || {};
                if(!_.isUndefined(path.prev)){
                    this.prevQuestion = path.prev[answer] || path.prev['default'] || undefined;
                }else {
                    this.prevQuestion = currentQuestionID;
                }

                if(!_.isUndefined(path.next)){
                    this.nextQuestion = path.next[answer] || path.next['default'] || undefined;
                }
            },

            proceed: function(event) {
                event.preventDefault();
                var button = $(event.currentTarget);
                var terminal = button.attr('data-next') == 'end' ? true : false;
                var parentQuestionID = button.parents('.question').attr('data-id');
                var answered = _.isUndefined(this.model.get(parentQuestionID)) ? false : true;
                var question = this.questionCollection.findWhere({id: parentQuestionID});

                if(!answered){
                    this.model.set(parentQuestionID, 'not answered');
                }

                if(terminal){
                    // last question submit form and show thank-you page
                    $('#survey-form').remove();
                    $('#survey-thank-you').removeClass('hidden');

                    var dataLayer = window.dataLayer || [{}];

                    dataLayer.push({'usability_survey': this.model.toJSON()});
                    dataLayer.push({'event': 'usability_survey_submit'});

                    //store on Drupal DB
                    var post_path = Drupal.settings.usability_survey.post_path;
                    var survey_data = this.model.toJSON();

                    survey_data['page'] = Drupal.settings.getQ || "";
                    $.post(post_path, survey_data);

                }else{
                    this.prevQuestion = this.prevQuestion || button.attr('data-prev') || button.parents('.question').attr('data-id');
                    this.nextQuestion = this.nextQuestion || button.attr('data-next');

                    if(button.hasClass('button-prev')){
                        $('.question').addClass('hidden');
                        $('.question-' + this.prevQuestion).removeClass('hidden');
                    }else if(button.hasClass('button-next')){
                        $('.question').addClass('hidden');
                        $('.question-' + this.nextQuestion).removeClass('hidden');
                    }else {
                        return false;
                    }
                }

            }

        });

        return new App({ model: new Backbone.Model() });

    },

    getResource: function(resourcePath) {
        var path = Drupal.settings.basePath + Drupal.settings.usability_survey.path + resourcePath;
        var deferred = $.Deferred();

        $.get(path).done(function(data) {
            deferred.resolve(data);
        }).fail(deferred.reject);

        return deferred.promise();
    }
}
