var gulp = require('gulp');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify');
var pump = require('pump');

gulp.task('concat_scripts', function() {
  return gulp.src([
      './node_modules/underscore/underscore.js',
      './node_modules/backbone/backbone-min.js',
      './app/app.js'
    ])
    .pipe(sourcemaps.init())
        .pipe(concat('app.js'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./dist/'));
});

gulp.task('compress', ['concat_scripts'], function (cb) {
    var options = {
        preserveComments: 'license'
    };

  pump([
        gulp.src('dist/app.js'),
        uglify(),
        gulp.dest('dist')
    ],
    cb
  );
});

gulp.task('dev', ['concat_scripts']);
gulp.task('default', [ 'compress']);
